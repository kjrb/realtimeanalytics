﻿define(['jquery', 'knockout', 'signalR', 'socketio'], function (jq, ko, signalR, socketio) {
    console.log("jQuery version: ", $.fn.jquery);
    console.log("Knockout version: ", ko.version);
    console.log("SignalR version: ", $.signalR.version);
    console.log("SocketIO version: ", io.version);
    //console.log("SignalR version: ", signalR.version);
    console.log("SocketIO version: ", socketio.version);

    var universalClient = (function () {
        var that = this;
        this.useSignalR = ko.observable();
        this.useNodejs = ko.observable();
        this.useIISnode = ko.observable();
        this.sendHandlers = [];
        
        var msg = ko.observable();
        var clicks = 0;
        
        this.logger = function () {
            this.log = function (message) {
                console.log(message);
            };
            return this;
        }();
        
        this.init = function (config) {
            that.useSignalR(config.useSignalR);
            that.useNodejs(config.useNodejs);
            that.useIISnode(config.useIISnode);

            if (that.useSignalR()) {
                that.sendHandlers.push(signalRInit(that.useSignalR));
            }
            if (that.useNodejs()) {
                that.sendHandlers.push(nodejsInit(that.useNodejs) );
            }
            if (that.useIISnode()) {
                that.sendHandlers.push(iisnodeInit(that.useIISnode) );
            }
        };
        
        function signalRInit(shouldSend) {
            var ok = false;
            var url = 'http://localhost/RealTimeSignalRServer/';
            var connection = $.hubConnection(url);
            connection.logging = true;
            var proxy = connection.createHubProxy('analyticshub');
            proxy.on('message', function (data) {
                processReceivedData(data);
            });
            connection.start()
                .done(function () {
                    that.logger.log('Now connected to ' + url + ', connection ID=' + connection.id + ' UniqueId=');
                    ok = true;
                })
                .fail(function() {
                    that.logger.log('Could not connect to ' + url);
                });

            that.disconnetHandler = function () {
                that.logger.log('Disconnected from ' + url);
                ok = false;
            };

            function send(data) {
                if (!ok || !shouldSend()) {
                    that.logger.log('Not sending to ' + url);
                    return;
                }
                
                proxy.invoke('send', data).done(function() {
                    that.logger.log('Invocation of send to ' + url + ' succeeded');
                }).fail(function(error) {
                    that.logger.log('Invocation of send to ' + url + ' failed. Error: ' + error);
                });
            }

            return {
                send: send
            };
        }

        function nodejsInit(shouldSend) {
            var url = 'http://localhost:8888/analyticshub';
            return nodejsCommon(url, shouldSend);
        }

        function iisnodeInit(shouldSend) {
            var url = 'http://localhost:9999/analyticshub';
            return nodejsCommon(url, shouldSend);
        }

        function nodejsCommon(url, shouldSend) {
            var ok = false;
            var client = io.connect(url, { 'sync disconnect on unload': true });

            client.on('message', function (data) {
                processReceivedData(data);
            });

            client.on('connect', function () {
                that.logger.log('Now connected to ' + url + ', connection ID=' + client.socket.sessionid + ' UniqueId=');
                ok = true;
            });

            client.on('connect_failed', function () {
                that.logger.log('Could not connect to ' + url);
            });

            that.disconnetHandler = function () {
                that.logger.log('Disconnected from ' + url);
                ok = false;
                client.disconnect();
            };
            
            function send(data) {
                if (!ok || !shouldSend()) {
                    that.logger.log('Not sending to ' + url);
                    return;
                }
                client.emit('send', data);
                that.logger.log('Invocation of send to ' + url + ' succeeded');
            }

            return {
                send: send
            };
        }

        function processReceivedData(data) {
            if (data.type === 'greeting') {
                that.logger.log('unique id: ' + data.message);
            }
        }
        
        function buildMessage() {
            clicks++;
            msg(('Time: ' + new Date()).valueOf() + ' Clicks: ' + clicks);
        }

        function sendMessage() {
            $.each(that.sendHandlers, function (i, handler) {
                var toSend = msg();
                if (handler && handler.send && $.isFunction(handler.send)) {
                    handler.send(toSend);
                }
            });
           
        }

        return {
            init: init,
            sendData: function() {
                buildMessage();
                sendMessage();
            },
            message: msg
        };
    })();

    return universalClient;
});